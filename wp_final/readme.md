
## Design
* [API design](docs/api.md)
* [Docs for Install](docs/install_docs.md)

## Tests:
Testing will be utilized with the unittest library.
Testing consists of, ensuring that the API is returning a 200 response, and dependencies have been installed. 

To run the test file:
`python -m unittest tests/test_view.py`


## Functionality:
This program contains a CLI command to run a Python file that will take in an input CSV file as the first argument, and output a new CSV file that has been merged with an API to provide additional data for Account's. The new CSV file contains all original pieces of data on the input file, with two additional headers from the API. 

We will primarily use the built in functions of Python. We'll be importing CSV to read the files. JSON and Requests to read the data coming from the API. Followed by SYS to run command line arguments and pass them to the function. 


## Develop:
- Setting up this program only requires a few things to get running.
- You must have Python installed. 

1. Ensure that your .csv file is in the root of the project and not inside of a folder. 

2. Python offers a built-in virtual environment so you can install dependencies just in the virtual environment, rather than to your local machine. 
This is recommended not only for efficiency but security as well. 
This is best typed in your real terminal and not an IDE terminal.

In your terminal inside of the folder where the application exists. 

Create the venv file:
Run `python -m venv .venv` 

Start the virtual environment file:
macOS:
`source ./.venv/bin/activate`

Windows:
`.\.venv\Scripts\Activate.ps1`

To exit the venv:
run `deactivate`


## Running on Windows:

After the venv is running; 
Run `pwd` to grab the pathway for environment variables. 
Copy that directory and add it to your System Environment Variables within "PATH". 
From your start menu, search for and open `git bash` 
{windows does not automatically associate .sh files, manually opening git bash will ensure that there isn't any errors due to a non-configured relationship}
After you navigate to the folder where the project is located;
Run `run.sh` to install all dependencies for the project. 

<!-- ready to run the merge for windows -->

## Running on MacOS or Linux:
Once your virtual environment is running in your terminal; 
Run `run.sh`
~ if you recieve a permissions error; 
manually run `sudo chmod +rwx run.sh`

You may be prompted to give a password to give permissions for the .sh files
The run file will then install dependencies, and set the path variable for you. It will then notify you of completion. 
~ if when checking `echo $PATH` it does not return your file name in the path or you run into an error where it says wpe_merge 'command not found'; 
manually add it by running, `export PATH=$PATH:$pwd`


<!-- linux will set the path and permissions -->

<!-- OS may need to manually set path  -->


3. Once path is correctly configured, simply run this command in your terminal of choice. The first property is an .sh file that executes the program. The second property is an argument that needs to be the title of your .csv file. The third property is what you would like your new .csv file to be named, as an argument that gets passed in. 

` wpe_merge <inputfile.csv> <outputfile.csv> `

    When merge is successful; you should see the `outputfile.csv` file at the root level. You'll also see a `Merge Complete` message to let you know it was successful. 


## Happy Merging!

